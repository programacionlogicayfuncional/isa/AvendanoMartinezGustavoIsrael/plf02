(ns plf02.core)

;Predicados
(defn funcion-asosiative?-1
  [a] 
  (associative? a))

(defn funcion-asosiative?-2
  [a] 
  (associative? a))

(defn funcion-asosiative?-3

  [a] 
  (associative? a))

(funcion-asosiative?-1 [1 2 3])
(funcion-asosiative?-2 '(1 2 3))
(funcion-asosiative?-3 {:a 1 :b 2})



(defn funcion-boolean?-1
  [a] 
  (boolean? a))

(defn funcion-boolean?-2
  [a] 
  (boolean? a))

(defn funcion-boolean?-3
  [a] 
  (boolean? a))

(funcion-boolean?-1 1)
(funcion-boolean?-2 true)
(funcion-boolean?-3 false)


(defn funcion-char?-1
  [a] 
  (char? a))

(defn funcion-char?-2
  [a] 
  (char? a))

(defn funcion-char?-3
  [a] 
  (char? a))

(funcion-char?-1 \a)
(funcion-char?-2 #{\a})
(funcion-char?-3 0)


(defn función-coll?-1
  [a]
  (coll? a))

(defn función-coll?-2
  [a y]
  (coll? (+ a y)))

(defn función-coll?-3
  [a]
  (coll? a))

(función-coll?-1 [2 3 4])
(función-coll?-2 3 4)
(función-coll?-3 '("columna 0" "columna1"))



(defn función-decimal?-1
  [a y]
  (decimal? (+ a y)))

(defn función-decimal?-2
  [a]
  (decimal? a))

(defn función-decimal?-3
  [a]
  (decimal? a))

(función-decimal?-1 1M 3)
(función-decimal?-2 "hola")
(función-decimal?-3 [3 4 5 6])


(defn función-double?-1
  [a]
  (double? a))

(defn función-double?-2
  [a b]
  (double? (+ a b)))

(defn función-double?-3
  [a b]
  (double? (- a b)))


(función-double?-1 1.0)
(función-double?-2 5.5 3)
(función-double?-3 2.0 5.0)



(defn función-float?-1
  [a]
  (float? a))

(defn función-float?-2
  [a b]
  (float? (+ a b)))

(defn función-float?-3
  [a b]
  (float? (* a b)))

(función-float?-1 1.0)
(función-float?-2 3 2.1)
(función-float?-3 4 5)


(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a]
  (ident? a))

(defn función-ident?-3
  [a b]
  (ident? (conj a b)))

(función-ident?-1 'abc)
(función-ident?-1 :a)
(función-ident?-3 [1 2 3] :b)



(defn función-indexed?-1
  [a]
  (indexed? a))

(defn función-indexed?-2
  [a b]
  (indexed? (a b)))

(defn función-indexed?-3
  [a]
  (indexed? a))

(función-indexed?-1 [1 2 3]) 
(función-indexed?-2 ["hello" "israel"] 1)
(función-indexed?-3 [1 2 3 {:a 1 :b 2}])


(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [a b]
  (int? (/ a b)))

(defn función-int?-3
  [a b]
  (int? (* a b)))

(función-int?-1 25.0)
(función-int?-2 10 2)
(función-int?-3 3 4)

(defn función-integer?-1
  [a]
  (integer? a))

(defn función-integer?-2
  [a b]
  (integer? (* a b)))

(defn función-integer?-3
  [a b c]
  (integer? (- a b c)))

(función-integer?-1 11)
(función-integer?-2 3 5)
(función-integer?-3 3 5 7)


(defn función-keyword?-1
  [a]
  (keyword? a))

(defn función-keyword?-2
  [a]
  (keyword? a))

(defn función-keyword?-3
  [a]
  (keyword? a))


(función-keyword?-1 :a.b.c)
(función-keyword?-2 {:a 3 :b 5 :c 7})
(función-keyword?-3 {:a 3 :b 5})


(defn función-list?-1
  [a]
  (list? a))

(defn función-list?-2
  [a]
  (list? a))

(defn función-list?-3
  [a]
  (list? a))

(función-list?-1 '(1 2 3 4 5 6))
(función-list?-2 #{3 5 7 9 11})
(función-list?-3 '(3 5 7))


(defn función-map-entry?-1
  [a]
  (map-entry? a))

(defn función-map-entry?-2
  [a]
  (map-entry? a))

(defn función-map-entry?-3
  [a]
  (map-entry? a))

(función-map-entry?-1 {:a 3 :b 5 :c 6})
(función-map-entry?-2 {:a 3 :c 5})
(función-map-entry?-3 {:a 3 :b 5 :c 7})


(defn función-map?-1
  [a]
  (map? a))

(defn función-map?-2
  [a]
  (map? a))

(defn función-map?-3
  [a]
  (map? a))


(función-map?-1 {:a "hola" :b "20"})
(función-map?-2 (hash-map :a 1 :b 2))
(función-map?-3 '(1 2 3))

(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a]
  (nat-int? a))

(defn función-nat-int?-3
   [a]
  (nat-int? a))

(función-nat-int?-1 999)
(función-nat-int?-2 3)
(función-nat-int?-3 -9999)


(defn función-number?-1
  [a]
  (number? a))

(defn función-number?-2
  [a]
  (number? a))

(defn función-number?-3
  [a]
  (number? a))

(función-number?-1 3)
(función-number?-2 5)
(función-number?-3 [3 5 7 9 11])


(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [a]
  (pos-int? a))

(defn función-pos-int?-3
  [a]
  (pos-int? a))


(función-pos-int?-1 3579)
(función-pos-int?-2 3)
(función-pos-int?-3 -3579)


(defn función-ratio?-1
  [a]
  (ratio? a))

(defn función-ratio?-2
  [a]
  (ratio? a))

(defn función-ratio?-3
  [a]
  (ratio? a))


(función-ratio?-1 3/2)
(función-ratio?-2 2.5)
(función-ratio?-3 {1 3 5 7})


(defn función-rational?-1
  [a]
  (rational? a))

(defn función-rational?-2
  [a]
  (rational? a))

(defn función-rational?-3
  [a]
  (rational? a))

(función-rational?-1 3)
(función-rational?-2 5.0)
(función-rational?-3 -3.9)


(defn función-seq?-1
  [a]
  (seq? a))

(defn función-seq?-2
  [a]
  (seq? a))

(defn función-seq?-3
  [a]
  (seq? a))


(función-seq?-1 '(1 2 3))
(función-seq?-2 {3 5 7 1 11 19})
(función-seq?-3 [true false])


(defn función-seqable?-1
  [a]
  (seqable? a))

(defn función-seqable?-2
  [a]
  (seqable? a))

(defn función-seqable?-3
  [a]
  (seqable? a))

(función-seqable?-1 [])
((función-seqable?-2 nil)
(función-seqable?-3 [nil true false])
 
 
 (defn función-sequential?-1
   [a]
   (sequential? a))

(defn función-sequential?-2
  [a]
  (sequential? a))

(defn función-sequential?-3
  [a]
  (sequential? a))


(función-sequential?-1 [1 2 3 4 5 6])
(función-sequential?-2 '(1 3 5 6 7 9 11))
(función-sequential?-3 #{1 2 3 4 5})
 
 
(defn función-set?-1
  [a]
  (set? a))

(defn función-set?-2
  [a]
  (set? a))

(defn función-set?-3
  [a]
  (set? a))

(función-set?-1 #{1 2 3 4})
(función-set?-2 '(3 4 5) )
(función-set?-3 '(1 2 3 4 5 6))


(defn función-some?-1
  [a]
  (some? a))

(defn función-some?-2
  [a]
  (some? a))

(defn función-some?-3
  [a]
  (some? a))

(función-some?-1 45)
(función-some?-2 nil)
(función-some?-3 -5)



(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [a]
  (string? a))

(defn función-string?-3
  [a]
  (string? a))

(función-string?-1 "hola")
(función-string?-2 [1 2 3 4])
(función-string?-3 '("string1" "string2"))



(defn función-symbol?-1
  [a]
  (symbol? a))

(defn función-symbol?-2
  [a]
  (symbol? a))

(defn función-symbol?-3
  [a]
  (symbol? a))


(función-symbol?-1 'a)
(función-symbol?-2 9)
(función-symbol?-3 ['a 'b 'c 'd])

(defn función-vector?-1
  [a]
  (vector? a))

(defn función-vector?-2
  [a]
  (vector? a))

(defn función-vector?-3
  [a]
  (vector? a))

(función-vector?-1 ["Israel" "Avendaño Martinez"])
(función-vector?-2 (vector 1 2 3))
(función-vector?-3 '(1 2 3 4 5 6 7))


;Funciones de Orden superior
(defn funcion-drop-1 
  [a b]
  (drop a b))

(defn funcion-drop-2
  [a b]
  (drop a b))

(defn funcion-drop-3
   [a b]
  (drop a b))

(funcion-drop-1 3 [ 3 5 7 9 11 15 17])
(funcion-drop-2 5 '(4 5 6 89 2 4 7 8))
(funcion-drop-3 7 #{\a \b \c \d \e \f \g \h})


(defn funcion-drop-last-1
  [a] 
  (drop-last a))

(defn funcion-drop-last-2
  [a b] 
  (drop-last a b))

(defn funcion-drop-last-3
  [a b] 
  (drop-last a b))

(funcion-drop-last-1 [3 5 7 9 11 15 17 21])
(funcion-drop-last-2 2 '("uno" "dos" "tres" "cuatro" "cinco" "seis"))
(funcion-drop-last-3 2 [1 2 3 4 5 6])


(defn funcion-dropWhile-1
  [a b] 
  (drop-while a b))

(defn funcion-dropWhile-2
  [a b] 
  (drop-while a b))

(defn funcion-dropWhile-3
  [a b] 
  (drop-while a b))

(funcion-dropWhile-1 neg? [-1 0 1 2 3 4])
(funcion-dropWhile-2 pos? [1 0 -1 -2 -3 -4])
(funcion-dropWhile-3 char? #{\a \b \c 2 4})


(defn funcion-every?-1
  [a b]
  (every? a b))

(defn funcion-every?-2
  [a b]
  (every? a b))

(defn funcion-every?-3
  [a b]
  (every? a b))

(funcion-every?-1  even? '(1 3 5 7 9 11))
(funcion-every?-2 odd? '[1 3 5 7 9 11])
(funcion-every?-3 even? #{2 4 6 8 10})

(defn funcion-filterv-1
  [a b]
  (filterv a b))

(defn funcion-filterv-2
  [a b]
  (filterv a b))

(defn funcion-filterv-3
  [a b]
  (filterv a b))

(funcion-filterv-1 even? (range 11))
(funcion-filterv-2 odd? '(1 3 5 7 9 11))
(funcion-filterv-3 nil? #{1 3 5 7 9 11})

(defn funcion-groupBy-1
  [a b] 
  ( group-by a b))

(defn funcion-groupBy-2
  [a b]
  (group-by a b))

(defn funcion-groupBy-3
  [a b] 
  (group-by a b))

(funcion-groupBy-1 nil? (range 11))
(funcion-groupBy-2 odd? '(1 3 5 7 9 11))
(funcion-groupBy-3 even? #{1 3 5 7 9 11})


(defn funcion-iterate-1
  [a b] 
  (iterate a b))

(defn funcion-iterate-2
  [a b] 
  (iterate a b))

(defn funcion-iterate-3
  [a b] 
  (iterate a b))
 
(funcion-iterate-1 inc 6)
(funcion-iterate-2 dec 5)
(funcion-iterate-3 inc 2)


(defn funcion-keep-1
  [a b]
  (keep a b))

(defn funcion-keep-2
  [a b]
  (keep a b))

(defn funcion-keep-3
  [a b]
  (keep a b))

(funcion-keep-1 { :a 1 :b 2 :c 3 } [ :a :b :d ] )
(funcion-keep-2 {:a 1 :b 2 :c 3} [:a 4 :b 5 :d 6])
(funcion-keep-3 {:a 3 :b 4} [:a 5 :d 6])


(defn funcion-keepIndexed-1
  [a] 
  (keep-indexed a))

(defn funcion-keepIndexed-2
  [a b] 
  (keep-indexed a b))

(defn funcion-keepIndexed-3
  [a] 
  (keep-indexed a))

(funcion-keepIndexed-1 '{\a \b \c \d \e \f})
(funcion-keepIndexed-2 4 [:a 5 :d 6]))
(funcion-keepIndexed-3 5)


(defn funcion-mapIndexed-1
  [a b] 
  (map-indexed a b))

(defn funcion-mapIndexed-2
  [a b] 
  (map-indexed a b))

(defn funcion-mapIndexed-3
  [a b] 
  (map-indexed a b))

(funcion-mapIndexed-1 vector "Israel Avendaño")
(funcion-mapIndexed-2 vector #{\a \b \c \d \e})
(funcion-mapIndexed-3 vector '(6 52 4 1 4 0))

(defn funcion-mapcat-1
  [a b c]
  (mapcat a b c))

(defn funcion-mapcat-2
  [a b c]
  (mapcat a b c))

(defn funcion-mapcat-3
    [a b c]
  (mapcat a b c))

(funcion-mapcat-1 list [:a :b :c] [1 2 3])
(funcion-mapcat-2 list [:a :b] [1 2 3 4 5])
(funcion-mapcat-3 list [:a :c] [1 2 3])

(defn funcion-mapv-1
  [a b]
  (mapv a b))

(defn funcion-mapv-2
  [a b c]
  (mapv a b c))

(defn funcion-mapv-3
  [a b c]
  (mapv a b c))

(funcion-mapv-1 inc [1 2 3 4 5])
(funcion-mapv-2 + [1 2 3] (iterate inc 1))
(funcion-mapv-3 + [1 2 3] [4 5 6])


(defn funcion-merge-with-1
  [a b c] 
  (merge-with a b c))

(defn funcion-merge-with-2
  [a b c] 
  (merge-with a b c))

(defn funcion-merge-with-3
  [a b c] 
  (merge-with a b c))

(funcion-merge-with-1 + {:a 1 :b 3} {:a 4 :b 2})
(funcion-merge-with-2 *{:a 1 :b 3} {:a 4 :b 2 :c 5})
(funcion-merge-with-3 into {"uno" ["a" "b"] "dos" ["d" "e" "f"]}
                      {"uno" ["1" "2"] "dos" ["4" "5" "6"]})

(defn funcion-not-any?-1
  [a b] 
  (not-any? a b))

(defn funcion-not-any?-2
  [a b] 
  (not-any? a b))

(defn funcion-not-any?-3
  [a b] 
  (not-any? a b))

(funcion-not-any?-1 odd? '( 4 2 6))
(funcion-not-any?-2 odd? (range 11))
(funcion-not-any?-3 nil? [1 3 5])


(defn funcion-not-every?-1
  [a b]
  (not-every? a b))

(defn funcion-not-every?-2
  [a b]
  (not-every? a b))

(defn funcion-not-every?-3
  [a b]
  (not-every? a b))

(funcion-not-every?-1 nil? '(4 5 1 3 7))
(funcion-not-every?-2 odd? [1 3 5 7])
(funcion-not-every?-3 even? (range 11))

(defn funcion-parttition-by-1
  [a b]
  (partition-by b a))

(defn funcion-parttition-by-2
  [a b]
  (partition-by a b))

(defn funcion-parttition-by-3
  [a b]
  (partition-by a b))

(funcion-parttition-by-1 [1 3 5 9 11 15 17 21] #(= 2 %))
(funcion-parttition-by-2 '(1 2 3 4 5) #(= 3 %))
(funcion-parttition-by-3 (range 10) #(= 4 %))

(defn funcion-reduce-kv-1
  [a b c] 
  (reduce-kv a b c))

(defn funcion-reduce-kv-2
  [a b c] 
  (reduce-kv a b c))

(defn funcion-reduce-kv-3
  [a b c] 
  (reduce-kv a b c))

(funcion-reduce-kv-1 #(assoc %1 %3 %2) {} {:a 1 :b 2 :c 3})
(funcion-reduce-kv-2 (fn [res idx itm] (assoc res idx itm)) {} ["one" "two" "three"])
(funcion-reduce-kv-3 #(assoc %1 %2 %3) {} {:a 1 :b 4 :c 5})


(defn funcion-remove-1
  [a b]
  (remove a b))

(defn funcion-remove-2
  [a b]
  (remove a b))

(defn funcion-remove-3
  [a b]
  (remove a b))

(funcion-remove-1 pos? [-1 -2 2 -1 3 7 0])
(funcion-remove-2 neg? [1 -2 2 -1 3 7 0])
(funcion-remove-3 pos? '(1 -2 2 -1 3 7 0))

(defn funcion-reverse-1
  [a]
  (reverse a))

(defn funcion-reverse-2
  [a]
  (reverse a))

(defn funcion-reverse-3
  [a]
  (reverse a))

(funcion-reverse-1 [6 5 4 3 2 1 0])
(funcion-reverse-1 '(-1 9 8 7 -2 4))
(funcion-reverse-1 (range 11))


(defn funcion-some-1
  [a b]
  (some a b))

(defn funcion-some-2
  [a b]
  (some a b))

(defn funcion-some-3
  [a b]
  (some a b))

(funcion-some-1 true? '(1 3 5 7 9))
(funcion-some-2 even? [0 1 3 5 7 9 -1])
(funcion-some-3 #(= 5 %) (range 11))

(defn funcion-sort-by-1
  [a b]
  (sort-by a b))

(defn funcion-sort-by-2
  [a b c]
  (sort-by a b c))

(defn funcion-sort-by-3
  [a b]
  (sort-by a b))

(funcion-sort-by-1 count ["aaa" "bb" "cccc"])
(funcion-sort-by-2 val > {:foo 7, :bar 3, :baz 5})
(funcion-sort-by-3 count '("aaa" "bb" "c"))


(defn funcion-split-with-1
  [a b] 
  (split-with a b))

(defn funcion-split-with-2
  [a b] 
  (split-with a b))

(defn funcion-split-with-3
  [a b] 
  (split-with a b))

(funcion-split-with-1 (partial >= 1) [1 3 5 7 9 11])
(funcion-split-with-1 (partial > 3) '(1 3 5 7 9 11))
(funcion-split-with-1 (partial > 10) (range 11))

(defn funcion-take-1
  [a b]
  (take a b))

(defn funcion-take-2
  [a b]
  (take a b))

(defn funcion-take-3
  [a b]
  (take a b))

(funcion-take-1 3 '(3 5 7 9 11 15 17 21))
(funcion-take-2 5 [3 5 7 9 11 15 17 21])
(funcion-take-3 7 (range 11))


(defn funcion-take-last-1
  [a b]
  (take-last a b))

(defn funcion-take-last-2
  [a b]
  (take-last a b))

(defn funcion-take-last-3
  [a b]
  (take-last a b))

(funcion-take-last-1 3 '(3 5 7 9 11 15 21))
(funcion-take-last-2 5 [21 15 11 9 7 5 3])
(funcion-take-last-3 7 (range 11))


(defn funcion-take-nth-1
  [a b] 
  (take-nth a b))

(defn funcion-take-nth-2
  [a b] 
  (take-nth a b))

(defn funcion-take-nth-3
  [a b] 
  (take-nth a b))

(funcion-take-nth-1 3 '(3 5 7 9 11 15 21 23))
(funcion-take-nth-2 2 [23 21 15 11 9 7 5 3])
(funcion-take-nth-3 3 (range 21))


(defn funcion-take-while-1
  [a b] 
  (take-while a b))

(defn funcion-take-while-2
  [a b] 
  (take-while a b))

(defn funcion-take-while-3
  [a b] 
  (take-while a b))

(funcion-take-while-1 pos? [-9 -5 -3 1 0 1 3 5 9])
(funcion-take-while-1 neg? '(-9 -5 -3 1 0 1 3 5 9))
(funcion-take-while-1 #(> 1 %) [-2 -1 0 1 2 3 4])


(defn funcion-update-1
  [a b c] 
  (update a b c))

(defn funcion-update-2
  [a b c] 
  (update a b c))

(defn funcion-update-3
  [a b c] 
  (update a b c))

(funcion-update-1 {:name "Israel" :age 12} :age inc)
(funcion-update-1 {:name "David"  :age 13} :age dec)
(funcion-update-1 {:name "Angel"  :age 14} :age inc)


(defn funcion-update-in-1
  [a b c]
  (update-in a b c))

(defn funcion-update-in-2
  [a b c]
  (update-in a b c))

(defn funcion-update-in-3
  [a b c]
  (update-in a b c))

(funcion-update-in-1 [{:name "Israel" :age 12}  {:name "Elias   " :age 22}] [1 :age] inc)
(funcion-update-in-2 [{:name "David " :age 13}  {:name "Dulce   " :age 23}] [0 :age] dec)
(funcion-update-in-3 [{:name "Angel " :age 14}  {:name "Michelle" :age 24} {:name "Columba" :age 28}] [2 :age] inc)

; Israel Avendaño 